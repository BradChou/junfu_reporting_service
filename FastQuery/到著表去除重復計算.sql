declare @Date datetime 

set @Date = '2020-05-26'

declare @table_var table
(
	[station] varchar(20),
	[tsp_code] varchar(20),
	check_number varchar(20) INDEX ndex_empName (check_number )
)

insert into @table_var
select 
	distinct
	isnull(t.station_name, '���cmomo') as [station], 
	r.supplier_code as [tsp_code],  
	r.check_number

from ttDeliveryScanLog s 
join tcDeliveryRequests r on s.check_number = r.check_number 
left outer join tbStation t on r.supplier_code = t.station_code
where s.scan_date between @Date and DateAdd("d", 1, @Date) and s.scan_item in ('5', '6','7')  and r.send_contact not like '���t�t%'

declare @table_var_3 table
(
	[station] varchar(20),
	[tsp_code] varchar(20),
	check_number varchar(20) INDEX ndex_empName (check_number ),
	scan_date datetime
)

insert into @table_var_3
select max(t.station) as station , max(t.tsp_code) as tsp_code,  t.check_number , min(d.scan_date) as scan_date from @table_var t join ttDeliveryScanLog d on t.check_number = d.check_number group by t.check_number having min(d.scan_date) > @Date



declare @table2 table(
	[station_id] int null,
	[station] varchar(20),
	[tsp_code] varchar(20),
	[count] int,
	[dayuan_count] int
)

insert into @table2 
select
	(select id from tbStation where station_code = tsp_code) as [station_id],
	t.station as [station],
	t.tsp_code,
	count(*) as [count],
	0

from @table_var_3 t
where len(t.check_number) = 12 and t.check_number not like '102%' and t.check_number not like '99%'
group by t.tsp_code, t.station

update @table2 
set dayuan_count = count
where station_id between 1 and 9

select * from @table2 
order by tsp_code

--select check_number, count(*) from @table_var group by check_number having count(*) > 1

