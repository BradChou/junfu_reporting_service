/*零擔各客戶配送數*/
select send_contact as send_contact_name,
	   sum(pieces) as order_with_scan_log
from tcDeliveryRequests
where (print_date>= '2020-05-05' and print_date <DATEADD(day,1,'2020-05-05')) and
	  (check_number not between '102000000000' AND '103000000000')  and
	  (check_number < '990000000000') and
	  LEN(check_number)=12 and
	  check_number in (select check_number from ttDeliveryScanLog)
group by send_contact
order by  count(send_contact) desc
go


select send_contact,
	    sum(pieces) as order_created
from tcDeliveryRequests
where (print_date>= '2020-05-05' and print_date <DATEADD(day,1,'2020-05-05')) and
	  (check_number not between '102000000000' AND '103000000000')  and
	  (check_number < '990000000000') and
	  LEN(check_number)=12
group by send_contact
order by  count(send_contact) desc
go