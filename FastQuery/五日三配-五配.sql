﻿declare @BeginDate datetime
set @BeginDate = '2020-06-25'

declare @EndDate datetime
set @EndDate = '2020-06-25'

declare @TempDate datetime
set @TempDate = @BeginDate

declare @table_var table 
(   
	scan_date date,
	check_number varchar(50), 
	arrive_option varchar(20), 
	ship_date datetime
)
​
declare @table_var1 table
(
	scan_date date,
	check_number varchar(50),
	arrive_option varchar(20)
)

WHILE @TempDate <= @EndDate
BEGIN

insert into @table_var  
select convert(date,scan_date,101) as scan_date
       , DR.check_number
	   , max(arrive_option) 
	   ,DR.ship_date
	   from ttDeliveryScanLog DS
	   inner join tcDeliveryRequests DR
       on DR.check_number = DS.check_number
	   where 
	    scan_item = '3' and arrive_option <> '3' and
	   (DR.ship_date>=@TempDate and DR.ship_date <DATEADD(day,1,@TempDate)) and
		DS.scan_date <=DATEADD(day,5,DR.ship_date ) and
        (DR.check_number not between '102000000000' AND '103000000000')  and
	    (DR.check_number < '990000000000') and
	    LEN(DR.check_number)=12 

	   GROUP BY scan_date, DR.check_number,DR.ship_date  
	   

SET  @TempDate=DATEADD(day,1,@TempDate ); 　
END;

insert into @table_var1     
select scan_date, check_number, max(arrive_option) as arrive_option  
from @table_var 
group by scan_date, check_number
​
​
declare @table_var2 table
(
	check_number varchar(50),
	scan_date1 varchar(50),
	arrive_option1 varchar(20),
	scan_date2 varchar(50),
	arrive_option2 varchar(20),
	scan_date3 varchar(50),
	arrive_option3 varchar(20),
	scan_date4 varchar(50),
	arrive_option4 varchar(20),
	scan_date5 varchar(50),
	arrive_option5 varchar(20)
)
​
​
--定義Cursor並打開
DECLARE MyCursor Cursor FOR --宣告，名稱為MyCursor
​
-- 此區段就可以撰寫你的資料集
select check_number, 
       scan_date, 
       case arrive_option
            when '1' then '客戶不在'
			when '2' then '約定再配'
			when '4' then '拒收'
			when '5' then '地址錯誤'
			when '6' then '查無此人'
		end as arrive_option
from @table_var1 order by check_number, scan_date
​
Open MyCursor 
​
​
print @@CURSOR_rows --查看總筆數
​
​
--定義所需變數
declare @check_number varchar(25), @scan_date varchar(50), @arrive_option varchar(50) 
​
--開始迴圈跑Cursor Start
Fetch NEXT FROM MyCursor INTO @check_number, @scan_date, @arrive_option
While (@@FETCH_STATUS <> -1)  --傳回int  -1:FETCH 陳述式失敗，或資料列已超出結果集。
BEGIN
​
DECLARE @scan_date1 varchar(50), @scan_date2 varchar(50),@scan_date3 varchar(50),@scan_date4 varchar(50),@scan_date5 varchar(50), @mycount int
​
select @mycount = count(*) from @table_var2 where check_number = @check_number
​
IF @mycount = 0 BEGIN
​
	insert into @table_var2 (check_number, scan_date1, arrive_option1) values (@check_number, @scan_date, @arrive_option)
​
END ELSE BEGIN
​
	select @scan_date2 = scan_date2, @scan_date3 = scan_date3, @scan_date4 = scan_date4, @scan_date5 = scan_date5 from @table_var2 where check_number = @check_number
​
	IF @scan_date2 is null BEGIN
​
		update @table_var2 set scan_date2 = @scan_date, arrive_option2 = @arrive_option where check_number = @check_number
	
	END ELSE IF @scan_date3 is null BEGIN
​
		update @table_var2 set scan_date3 = @scan_date, arrive_option3 = @arrive_option where check_number = @check_number
​
	END ELSE IF @scan_date4 is null BEGIN
​
		update @table_var2 set scan_date4 = @scan_date, arrive_option4 = @arrive_option where check_number = @check_number
​
	END ELSE IF @scan_date5 is null BEGIN
​
		update @table_var2 set scan_date5 = @scan_date, arrive_option5 = @arrive_option where check_number = @check_number
​
	END
​
END
​
Fetch NEXT FROM MyCursor INTO @check_number, @scan_date, @arrive_option
END
​
--開始迴圈跑Cursor End
​
--關閉&釋放cursor
CLOSE MyCursor
DEALLOCATE MyCursor
​
select 
check_number as '託運單號',
scan_date1 as '第一配掃讀時間',
arrive_option1 as '第一配掃讀配達區分',
scan_date2 as '第二配掃讀時間',
arrive_option2 as '第二配掃讀配達區分',
scan_date3 as '第三配掃讀時間',
arrive_option3 as '第三配掃讀配達區分',
scan_date4 as '第四配掃讀時間',
arrive_option4 as '第四配掃讀配達區分',
scan_date5 as '第五配掃讀時間',
arrive_option5 as '第五配掃讀配達區分'
from @table_var2 where scan_date3 is not null
​
--select * from @table_var order by check_number
​
--select count(distinct scan_date),  check_number from @table_var group by check_number having count(distinct scan_date) > 3
​
--select arrive_option, * from ttDeliveryScanLog where check_number = '100000380630' and scan_item = '3' order by scan_date
