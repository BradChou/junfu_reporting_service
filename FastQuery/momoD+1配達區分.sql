declare @start_date datetime
declare @end_date datetime
set @start_date = '2020-05-01'
set @end_date = '2020-05-26'

declare @table1 table(
print_date datetime, 
day_in_week int, 
day_in_week_char nvarchar(3),
check_number nvarchar(20),
aar_code nvarchar(15)
)

insert into @table1 
select 
dr.print_date, 
datepart(dw, dr.print_date), 
null,
dr.check_number, 
dr.area_arrive_code
from tcDeliveryRequests as dr
where dr.customer_code = 'F3500010002' 
and dr.print_date >= @start_date and dr.print_date <= @end_date 

update @table1 
set day_in_week_char = '日' 
where day_in_week = 1
update @table1 
set day_in_week_char = '一' 
where day_in_week = 2
update @table1 
set day_in_week_char = '二' 
where day_in_week = 3
update @table1 
set day_in_week_char = '三' 
where day_in_week = 4
update @table1 
set day_in_week_char = '四' 
where day_in_week = 5
update @table1 
set day_in_week_char = '五' 
where day_in_week = 6
update @table1 
set day_in_week_char = '六' 
where day_in_week = 7

declare @table2 table(
print_date datetime , 
day_in_week_char nvarchar(3),
scan_time datetime, 
check_number nvarchar(20),
aar_code nvarchar(15), 
ar_option varchar(2)
)

insert into @table2
select 
t.print_date,
t.day_in_week_char,
sl.scan_date,
t.check_number,
t.aar_code,
sl.arrive_option 

from @table1 as t
join ttDeliveryScanLog as sl 
on t.check_number = sl.check_number 
where scan_date >= dateadd(d, 1, t.print_date) and scan_date < dateadd(d, 2, t.print_date)
and scan_item = 3

declare @table3 table(
print_date datetime, 
day_in_week nvarchar(3), 
scan_time datetime, 
check_number nvarchar(20),
aar_code nvarchar(15),
ar_option int,
option_name nvarchar(10)
)

insert into @table3 
	select
		t.print_date,
		t.day_in_week_char,
		r.st,
		t.check_number,
		t.aar_code,
		t.ar_option,
		null 
	from
	(select
		check_number, 
		max(scan_time) as st
	from @table2 t group by check_number) r 
	inner join @table2 t 
	on r.check_number = t.check_number and r.st = t.scan_time


update @table3 
set option_name = '未配'
where ar_option is null
update @table3 
set option_name = '簽單留置'
where ar_option = 0
update @table3 
set option_name = '客戶不在'
where ar_option = 1
update @table3 
set option_name = '約定再配'
where ar_option = 2
update @table3 
set option_name = '正常配交'
where ar_option = 3
update @table3 
set option_name = '拒收'
where ar_option = 4
update @table3 
set option_name = '地址錯誤'
where ar_option = 5
update @table3 
set option_name = '查無此人'
where ar_option = 6

select 
	format(print_date, 'yyyy-MM-dd') as [發送日期],
	day_in_week as [星期], 
	format(scan_time, 'yyyy-MM-dd hh:mm') as [掃讀時間], 
	check_number as [貨號],
	isnull(aar_code, '未分區') as [到著站所代碼], 
	option_name as [配達區分]
from @table3 
order by scan_time